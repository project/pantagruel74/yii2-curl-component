<?php

namespace Pantagruel74\Yii2CurlComponentTestUnit;

use Pantagruel74\Yii2CurlComponent\service\CurlService;
use Pantagruel74\Yii2Loader\Yii2Loader;
use PHPUnit\Framework\TestCase;

class CurlTest extends TestCase
{
    /**
     * @param string|null $name
     * @param array $data
     * @param $dataName
     */
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        Yii2Loader::load();
        parent::__construct($name, $data, $dataName);
    }

    /**
     * @return void
     */
    public function testCurl(): void
    {
        $service = new CurlService();
        $response = $service->get(
            'http://www.thomas-bayer.com/sqlrest/',
            [],
            false
        )->asText();
        $this->assertNotFalse(stripos($response, 'xml version="1.0"'));
    }

    public function testPost(): void
    {
        $service = new CurlService();
        $response = $service->post(
            'http://www.thomas-bayer.com/sqlrest/',
            [],
            false
        )->asText();
        $this->assertNotFalse(stripos($response, 'HTTP Status 500 – Internal Server Error'));
    }
}