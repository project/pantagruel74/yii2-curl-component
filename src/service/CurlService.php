<?php

namespace Pantagruel74\Yii2CurlComponent\service;

use Pantagruel74\Yii2CurlComponent\model\CurlResponse;
use yii\base\Model;

class CurlService extends Model
{
    const CLIENT_NO = 0;
    const CLIENT_BOT = 1;
    const CLIENT_FIREFOX = 2;

    public $connectionTimeOut = 30;
    public $client = 0;
    public $verbose = 0;

    /**
     * @param string $url
     * @param array $params
     * @param bool $ssl
     * @param bool $header
     * @return CurlResponse
     */
    public function get(
        string $url,
        array $params,
        bool $ssl = false,
        bool $header = false
    ): CurlResponse {
        $this->verbose("init curl..");
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url . ($params ? '?' . http_build_query($params) : ''));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        if($header) {
            curl_setopt($curl, CURLOPT_HEADER, 1);
        }
        curl_setopt($curl, CURLOPT_POST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, $ssl);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl,CURLOPT_CONNECTTIMEOUT,$this->connectionTimeOut);
        $this->setClient($curl);
        $this->verbose("executing curl..");
        $result = curl_exec($curl);
        $this->verbose("closing curl..");
        curl_close($curl);
        $this->verbose("return response..");
        return new CurlResponse($result);
    }

    /**
     * @param string $url
     * @param array $params
     * @param bool $ssl
     * @param bool $header
     * @return CurlResponse
     */
    public function post(
        string $url,
        array $params,
        bool $ssl = false,
        bool $header = false
    ): CurlResponse {
        $this->verbose("init curl..");
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        if($header) {
            curl_setopt($curl, CURLOPT_HEADER, 1);
        }
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, $ssl);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl,CURLOPT_CONNECTTIMEOUT,$this->connectionTimeOut);
        $this->setClient($curl);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            "Content-Type: application/json",
        ]);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($params,JSON_UNESCAPED_UNICODE));
        $this->verbose("executing curl..");
        $result = curl_exec($curl);
        $this->verbose("closing curl..");
        curl_close($curl);
        $this->verbose("return response..");
        return new CurlResponse($result);
    }

    /**
     * @param $curl
     * @return void
     */
    protected function setClient($curl): void
    {
        if(empty($this->client)) {
            return;
        } elseif (is_numeric($this->client)) {
            $clientConfig = [
                1 => 'Bot 1.0',
                2 => "Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0",
            ];
            if(!isset($clientConfig[$this->client])) {
                throw new \InvalidArgumentException("Unknown curl client value: " . strval($this->client));
            }
            curl_setopt($curl,CURLOPT_USERAGENT,$clientConfig[$this->client]);
        } else {
            curl_setopt($curl,CURLOPT_USERAGENT, $this->client);
        }
    }

    /**
     * @param string $msg
     * @return void
     */
    protected function verbose(string $msg): void
    {
        if(!empty($this->verbose)) {
            echo $msg . "\n";
        }
    }
}