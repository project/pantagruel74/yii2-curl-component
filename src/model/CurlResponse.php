<?php

namespace Pantagruel74\Yii2CurlComponent\model;

class CurlResponse
{
    /* @var string $text */
    public $text;

    /**
     * @param $text
     */
    public function __construct($text)
    {
        $this->text = $text ?? '';
    }

    /**
     * @return string
     */
    public function asText(): string
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function asPure()
    {
        return $this->text;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return !is_bool($this->text);
    }

    /**
     * @return array
     * @throws \ErrorException
     */
    public function asJsonDecoded(): array
    {
        $result = json_decode($this->text, true);
        if(is_null($result)) {
            throw new \ErrorException('Json parsing error: ' . print_r($result, true));
        }
        return $result;
    }
}